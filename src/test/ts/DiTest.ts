import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| access.data.di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('Templ'));
            assert.ok(container.getClazz('I18nTempl'));
            assert.ok(container.getClazz('Notif'));
            assert.ok(container.getClazz('ArchNotif'));
            // beans
            assert.ok(container.getBean('templDao'));
            assert.ok(container.getBean('i18nTemplDao'));
            assert.ok(container.getBean('notifDao'));
            assert.ok(container.getBean('archNotifDao'));


        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
