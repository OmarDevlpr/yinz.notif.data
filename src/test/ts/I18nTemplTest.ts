// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import I18nTempl from "../../main/models/I18nTempl";
import Templ from "../../main/models/Templ";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const i18nTemplDao = container.getBean<TypeormDao<I18nTempl>>('i18nTemplDao');
const templDao = container.getBean<TypeormDao<Templ>>('templDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


describe('| notif.data.I18nTemplTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let templ = new Templ();
        templ.code = chance.string({ length: 10 });
        templ.name = chance.string({ length: 10 });
        templ.title = chance.string({ length: 10 });
        templ.message = chance.string({ length: 10 });
        templ.sourceMarkup = chance.pick(['HTML', 'MD'] as any);
        templ.targetMarkup = chance.pick(['HTML', 'TXT'] as any);

        templ = await templDao.create(trans, templ, {user: "__super_user__"});

        let i18nTempl = new I18nTempl();
        i18nTempl.ownerId = templ.id;
        i18nTempl.locale = chance.string({ length: 10 });
        i18nTempl.title = chance.string({ length: 10 });
        i18nTempl.message = chance.string({ length: 10 });
        
        let i18nTempl2 = new I18nTempl();        
        i18nTempl2.ownerId = templ.id;
        i18nTempl2.locale = chance.string({ length: 10 });
        i18nTempl2.title = chance.string({ length: 10 });
        i18nTempl2.message = chance.string({ length: 10 });        

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await i18nTemplDao.create(trans, i18nTempl, options);
        let createAllResult = await i18nTemplDao.createAll(trans, [i18nTempl2], options);
        let updateByFilterResult = await i18nTemplDao.updateByFilter(trans, { title: createResult.title }, { title: createResult.title }, options);
        let updateByIdResult = await i18nTemplDao.updateById(trans, { title: createResult.title }, createResult.id, options);
        let findAllResult = await i18nTemplDao.findAll(trans, options);
        let findByIdResult = await i18nTemplDao.findById(trans, createResult.id, options);
        let findByFilterResult = await i18nTemplDao.findByFilter(trans, { title: i18nTempl2.title }, options);
        let deleteByFilterResult = await i18nTemplDao.deleteByFilter(trans, { title: "dont" }, options);
        let deleteByIdREsult = await i18nTemplDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
