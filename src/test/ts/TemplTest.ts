// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Templ from "../../main/models/Templ";
import I18nTempl from "../../main/models/I18nTempl";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const templDao = container.getBean<TypeormDao<Templ>>('templDao');
const i18nTemplDao = container.getBean<TypeormDao<I18nTempl>>('i18nTemplDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


describe('| notif.data.TemplTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let templ = new Templ();
        templ.code = chance.string({length: 10});
        templ.name = chance.string({length: 10});
        templ.title = chance.string({length: 10});
        templ.message = chance.string({length: 10});
        templ.sourceMarkup = chance.pick(['HTML', 'MD'] as any);
        templ.targetMarkup = chance.pick(['HTML', 'TXT'] as any);                


        let templ2 = new Templ();
        templ2.code = chance.string({ length: 10 });
        templ2.name = chance.string({ length: 10 });
        templ2.title = chance.string({ length: 10 });
        templ2.message = chance.string({ length: 10 });
        templ2.sourceMarkup = chance.pick(['HTML', 'MD'] as any);
        templ2.targetMarkup = chance.pick(['HTML', 'TXT'] as any);        



        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await templDao.create(trans, templ, options);
        let createAllResult = await templDao.createAll(trans, [templ2], options);
        let updateByFilterResult = await templDao.updateByFilter(trans, { code: createResult.code }, { code: createResult.code }, options);
        let updateByIdResult = await templDao.updateById(trans, { code: createResult.code }, createResult.id, options);
        let findAllResult = await templDao.findAll(trans, options);
        let findByIdResult = await templDao.findById(trans, createResult.id, options);
        let findByFilterResult = await templDao.findByFilter(trans, { code: templ2.code }, options);
        let deleteByFilterResult = await templDao.deleteByFilter(trans, { code: "dont" }, options);
        let deleteByIdREsult = await templDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });

    it('| # (all dao crud functions with local) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let templ = new Templ();
        templ.code = chance.string({ length: 10 });
        templ.name = chance.string({ length: 10 });
        templ.title = chance.string({ length: 10 });
        templ.message = chance.string({ length: 10 });
        templ.sourceMarkup = chance.pick(['HTML', 'MD'] as any);
        templ.targetMarkup = chance.pick(['HTML', 'TXT'] as any);


        let templ2 = new Templ();
        templ2.code = chance.string({ length: 10 });
        templ2.name = chance.string({ length: 10 });
        templ2.title = chance.string({ length: 10 });
        templ2.message = chance.string({ length: 10 });
        templ2.sourceMarkup = chance.pick(['HTML', 'MD'] as any);
        templ2.targetMarkup = chance.pick(['HTML', 'TXT'] as any);

        let options = { user: "__super_user__", locale: 'en' };
        // 2. execute test

        let createResult = await templDao.create(trans, templ, options);
        let createAllResult = await templDao.createAll(trans, [templ2], options);
        let updateByFilterResult = await templDao.updateByFilter(trans, { code: createResult.code }, { code: createResult.code }, options);
        let updateByIdResult = await templDao.updateById(trans, { code: createResult.code }, createResult.id, options);
        let findAllResult = await templDao.findAll(trans, options);
        let findByIdResult = await templDao.findById(trans, createResult.id, options);
        let findByFilterResult = await templDao.findByFilter(trans, { code: templ2.code }, options);
        let deleteByFilterResult = await templDao.deleteByFilter(trans, { code: "dont" }, options);
        let deleteByIdREsult = await templDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

        // 3.1 locale fields
                
        // 3.1.1 create local fields 
        let i18nTempl = new I18nTempl();
        i18nTempl.ownerId = createResult.id;
        i18nTempl.locale = 'en';
        i18nTempl.message = chance.string({ length: 10 });
        i18nTempl.title = chance.string({ length: 10 });

        await i18nTemplDao.create(trans, i18nTempl, { user: "__super_user__" })
        
        // 3.1.2 one of the two has to have the local fields

        assert.ok(i18nTempl.message === findAllResult[0].message || findAllResult[1].message)
        assert.ok(i18nTempl.title === findAllResult[0].title || findAllResult[1].title)        
                        

    })

});
