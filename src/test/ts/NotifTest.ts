// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Notif from "../../main/models/Notif";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const notifDao = container.getBean<TypeormDao<Notif>>('notifDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;


describe('| notif.data.NotifTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let notif = new Notif();
        notif.ref  = chance.string({ length: 10 });
        notif.dest = chance.string({ length: 10 });
        notif.destType = chance.pick(["E", "I", "S"] as any);        
        notif.languageCode = chance.string({ length: 2 });
        notif.message = chance.string({ length: 50 });
        notif.procDate = chance.date();
        notif.procMessage = chance.string({ length: 50 });        
        notif.procStatus = chance.pick(['W', 'I', 'S', 'R', 'E'] as any);        
        notif.readyOn = chance.date();
        notif.reason = chance.string({length: 10});
        notif.subject = chance.string({length: 10});
        notif.model = { name: chance.string({length: 10})};

        let notif2 = new Notif();
        notif2.ref = chance.string({ length: 10 });
        notif2.dest = chance.string({ length: 10 });
        notif2.destType = chance.pick(["E", "I", "S"] as any);
        notif2.languageCode = chance.string({ length: 2 });
        notif2.message = chance.string({ length: 50 });
        notif2.procDate = chance.date();
        notif2.procMessage = chance.string({ length: 50 });
        notif2.procStatus = chance.pick(['W', 'I', 'S', 'R', 'E'] as any);
        notif2.readyOn = chance.date();
        notif2.reason = chance.string({ length: 10 });
        notif2.subject = chance.string({ length: 10 });
        notif2.model = { name: chance.string({ length: 10 }) };
        


        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await notifDao.create(trans, notif, options);
        let createAllResult = await notifDao.createAll(trans, [notif2], options);
        let updateByFilterResult = await notifDao.updateByFilter(trans, { ref: createResult.ref }, { ref: createResult.ref }, options);
        let updateByIdResult = await notifDao.updateById(trans, { ref: createResult.ref }, createResult.id, options);
        let findAllResult = await notifDao.findAll(trans, options);
        let findByIdResult = await notifDao.findById(trans, createResult.id, options);
        let findByFilterResult = await notifDao.findByFilter(trans, { ref: notif2.ref }, options);
        let deleteByFilterResult = await notifDao.deleteByFilter(trans, { ref: "dont" }, options);
        let deleteByIdREsult = await notifDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)        
        assert.deepStrictEqual(findByFilterResult[0].model , notif2.model)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)        
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
