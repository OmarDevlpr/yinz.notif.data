
import Container from "@yinz/container/ts/Container";
import Notif from "../models/Notif";
import ArchNotif from "../models/ArchNotif";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import { Logger } from "@yinz/commons";
import { TypeormDao } from "@yinz/commons.data";
import Templ from "../models/Templ";
import I18nTempl from "../models/I18nTempl";

const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>('logger');

    let accessManager = container.getBean<AccessManager>('accessManager');

    let i18nTemplDao = new TypeormDao<I18nTempl>({ model: I18nTempl, accessManager, logger });
    let templDao = new TypeormDao<Templ>({ model: Templ, accessManager, logger });
    let notifDao = new TypeormDao<Notif>({ model: Notif, accessManager, logger });
    let archNotifDao = new TypeormDao<ArchNotif>({ model: ArchNotif, accessManager, logger });


    container.setBean('i18nTemplDao', i18nTemplDao);
    container.setBean('templDao', templDao);
    container.setBean('notifDao', notifDao);
    container.setBean('archNotifDao', archNotifDao);


};

const registerClazzes = (container: Container) => {

    container.setClazz('I18nTempl', I18nTempl);
    container.setClazz('Templ', Templ);
    container.setClazz('Notif', Notif);
    container.setClazz('ArchNotif', ArchNotif);

};


export {
    registerBeans,
    registerClazzes
};