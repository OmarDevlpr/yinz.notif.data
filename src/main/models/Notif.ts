import { Model } from "@yinz/commons.data";
import { Column, Entity } from "typeorm";

@Entity()
export default class Notif extends Model {

    @Column()
    ref: string;

    @Column()
    dest: string;

    @Column({ enum: ["E", "I", "S"] })
    destType: "E" | "I" | "S";

    @Column({ nullable: true })
    languageCode: string;

    @Column({ nullable: true })
    message: string;

    @Column({ nullable: true, type: "timestamptz" })
    procDate: Date;

    @Column({ nullable: true, name: 'model' })
    private _model?: string;

    @Column({ nullable: true })
    procMessage: string;

    @Column({ enum: ['W', 'I', 'S', 'R', 'E'] })
    procStatus: 'W'| 'I'| 'S'| 'R'| 'E';

    @Column({ nullable: false, type: "timestamptz"})
    readyOn: Date;
    
    @Column({ nullable: true })
    reason: string;   

    @Column({ nullable: true })
    subject: string;   

    get model() {
        if ( this._model )
            return JSON.parse(this._model);
        return this._model;
    }

    set model(m: any) {
        this._model = JSON.stringify(m);
    }
    
}