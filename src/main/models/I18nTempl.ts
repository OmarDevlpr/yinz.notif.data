import { Model } from "@yinz/commons.data";
import { Column, Entity } from "typeorm";

@Entity()
export default class I18nTempl extends Model {

    @Column()
    locale: string;
    
    @Column({ nullable: true })
    title: string;

    @Column()
    message: string;    

    @Column()
    ownerId: number;
}