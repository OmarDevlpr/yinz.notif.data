import Notif from "./Notif";
import { Column, Entity } from "typeorm";

@Entity()
export default class ArchNotif extends Notif {

    @Column()
    archOn: Date;

}