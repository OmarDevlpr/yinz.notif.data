import { Model } from "@yinz/commons.data";
import { Column, Entity, TableInheritance} from "typeorm";

@Entity()
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export default class Templ extends Model {

    @Column()
    code: string;

    @Column({ nullable: true })
    name: string;

    @Column({ nullable: true })
    title: string;

    @Column()
    message: string;

    @Column({ enum: ['HTML', 'MD'], nullable: true })
    sourceMarkup: 'HTML' | 'MD';

    @Column({ enum: ['HTML', 'TXT'], nullable: true })
    targetMarkup: 'HTML' | 'TXT';
    
}